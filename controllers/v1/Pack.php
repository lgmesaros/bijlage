<?php
/**
 * @package    PHP Advanced API Guide
 * @author     Davison Pro <davisonpro.coder@gmail.com>
 * @copyright  2019 DavisonPro
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Davison\v1;

use Db;
use Davison\Route;
use Davison\Database\DbQuery;
use Davison\Wolf\Pack as PackObject;
use Davison\Util\ArrayUtils;
use Davison\Validate;

class Pack extends Route {
	public function getPacks() {
		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('pack.pack_id, pack.name, GROUP_CONCAT(wolf.name) AS wolfs');
		// Build FROM
		$sql->from('pack', 'pack');
		// LEFT JOIN
		$sql->leftJoin('wolf', 'wolf', 'pack.pack_id = wolf.pack_id');
		// Group By
		$sql->groupBy('pack.pack_id');

		$packs = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'packs' => $packs
		]);
	}

	public function addPack() {
		$api = $this->api;
		$payload = $api->request()->post(); 

		$name = ArrayUtils::get($payload, 'name');

		if (!Validate::isCatalogName($name)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid pack name'
			]);
		}

		$pack = new PackObject();
		$pack->name = $name;

		$ok = $pack->save();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to create pack'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Pack was created',
			'pack' => [
				'pack_id' => $pack->id,
				'name' => $pack->name
			]
		]);
	}

}	
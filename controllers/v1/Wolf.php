<?php
/**
 * @package    PHP Advanced API Guide
 * @author     Davison Pro <davisonpro.coder@gmail.com>
 * @copyright  2019 DavisonPro
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Davison\v1;

use Db;
use Davison\Route;
use Davison\Database\DbQuery;
use Davison\Wolf\Wolf as WolfObject;
use Davison\Wolf\Pack as PackObject;
use Davison\Util\ArrayUtils;
use Davison\Validate;

class Wolf extends Route {

	public function getWolfs() {
		$api = $this->api;

		// Build query
		$sql = new DbQuery();
		// Build SELECT
		$sql->select('wolf.*');
		// Build FROM
		$sql->from('wolf', 'wolf');
		$wolfs = Db::getInstance()->executeS($sql);

		return $api->response([
			'success' => true,
			'wolfs' => $wolfs
		]);
	}

	public function addWolf() {
		$api = $this->api;
		$payload = $api->request()->post(); 

		$name = ArrayUtils::get($payload, 'name');
		$birthdate = ArrayUtils::get($payload, 'birthdate');
		$lat = ArrayUtils::get($payload, 'lat');
		$lng = ArrayUtils::get($payload, 'lng');
		$pack_id = ArrayUtils::get($payload, 'pack_id');

		if (!Validate::isGenericName($name)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid wolf name'
			]);
		}

		if (!Validate::isDate($birthdate)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid birth date'
			]);
		}

		if (!Validate::isFloat($lat)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid latitude location'
			]);
		}

		if (!Validate::isFloat($lng)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid longitude location'
			]);
		}

		if(!Validate::isInt($pack_id)) {
			return $api->response([
				'success' => false,
				'message' => 'Enter a valid pack ID'
			]);
		}

		$pack = new PackObject( (int) $pack_id );
		if (!Validate::isLoadedObject($pack)) {
			return $api->response([
				'success' => false,
				'message' => 'The pack ID (' . $pack_id . ') does not exist'
			]);
		}

		$wolf = new WolfObject();
		$wolf->name = $name;
		$wolf->birthdate = $birthdate;
		$wolf->lat = (float) $lat;
		$wolf->lng = (float) $lng;
		$wolf->pack_id = $pack->id;

		$ok = $wolf->save();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to create wolf'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Wolf was created',
			'wolf' => [
				'wolf_id' => $wolf->id,
				'name' => $wolf->name,
				'birthdate' => $wolf->birthdate,
				'lat' => (float) $wolf->lat,
				'lng' => (float) $wolf->lng,
				'pack' => [
					'pack_id' => $pack->id,
					'name' => $pack->name,
				],
			]
		]);
	}

	public function getWolf( $wolfId ) {
		$api = $this->api;

		$wolf = new WolfObject( (int) $wolfId );
		if(!Validate::isLoadedObject($wolf)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Wolf was not found'
			]);
		}
		
		$pack = new PackObject( $wolf->pack_id );

		return $api->response([
			'success' => true,
			'message' => 'Wolf was found',
			'wolf' => [
				'wolf_id' => $wolf->id,
				'name' => $wolf->name,
				'birthdate' => $wolf->birthdate,
				'lat' => (float) $wolf->lat,
				'lng' => (float) $wolf->lng,
				'pack' => [
					'pack_id' => $pack->id,
					'name' => $pack->name
				],
			]
		]);
	}

	public function updateWolf($wolfId ) {
		$api = $this->api;
		$payload = $api->request()->post(); 

		$wolf = new WolfObject( (int) $wolfId );
		if(!Validate::isLoadedObject($wolf)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Wolf was not found'
			]);
		}

		if (ArrayUtils::has($payload, 'name')) {
			$name = ArrayUtils::get($payload, 'name');
			if ( !Validate::isGenericName($name) ) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid wolf name'
				]);
			}

			$wolf->name = $name;
		}

		if (ArrayUtils::has($payload, 'birthdate')) {
			$birthdate = ArrayUtils::get($payload, 'birthdate');
			if (!Validate::isDate($birthdate)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid birth date'
				]);
			}

			$wolf->birthdate = $birthdate;
		}

		if (ArrayUtils::has($payload, 'lat')) {
			$lat = ArrayUtils::get($payload, 'lat');
			if (!Validate::isFloat($lat)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid latitude location'
				]);
			}

			$wolf->lat = $lat;
		}

		if (ArrayUtils::has($payload, 'lng')) {
			$lng = ArrayUtils::get($payload, 'lng');
			if (!Validate::isFloat($lng)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid longitude location'
				]);
			}

			$wolf->lng = $lng;
		}

		if (ArrayUtils::has($payload, 'pack_id')) {
			$pack_id = ArrayUtils::get($payload, 'pack_id');
			if(!Validate::isInt($pack_id)) {
				return $api->response([
					'success' => false,
					'message' => 'Enter a valid pack ID'
				]);
			}

			$pack = new PackObject( (int) $pack_id );
			if (!Validate::isLoadedObject($pack)) {
				return $api->response([
					'success' => false,
					'message' => 'The pack ID (' . $pack_id . ') does not exist'
				]);
			}

			$wolf->pack_id = $pack->id;
		}

		$ok = $wolf->save();
		
		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to update wolf'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Wolf updated successfully'
		]);
	}

	public function deleteWolf( $wolfId ) {
		$api = $this->api;

		$wolf = new WolfObject( (int) $wolfId );
		if(!Validate::isLoadedObject($wolf)) {
			$api->response->setStatus(404);
			return $api->response([
				'success' => false,
				'message' => 'Wolf was not found'
			]);
		}

		$ok = $wolf->delete();

		if (!$ok) {
			return $api->response([
				'success' => false,
				'message' => 'Unable to delete wolf'
			]);
		}

		return $api->response([
			'success' => true,
			'message' => 'Wolf deleted successfully'
		]);
	}
}



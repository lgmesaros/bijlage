-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bijlage_db
DROP DATABASE IF EXISTS `bijlage_db`;
CREATE DATABASE IF NOT EXISTS `bijlage_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bijlage_db`;

-- Dumping structure for table bijlage_db.pack
DROP TABLE IF EXISTS `pack`;
CREATE TABLE IF NOT EXISTS `pack` (
  `pack_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table bijlage_db.pack: ~2 rows (approximately)
DELETE FROM `pack`;
/*!40000 ALTER TABLE `pack` DISABLE KEYS */;
INSERT INTO `pack` (`pack_id`, `name`) VALUES
  (1, 'The Anonymous'),
  (2, 'The Sniffers');
/*!40000 ALTER TABLE `pack` ENABLE KEYS */;

-- Dumping structure for table bijlage_db.wolf
DROP TABLE IF EXISTS `wolf`;
CREATE TABLE IF NOT EXISTS `wolf` (
  `wolf_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `birthdate` datetime NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `pack_id` int(11) NOT NULL,
  PRIMARY KEY (`wolf_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table bijlage_db.wolf: ~5 rows (approximately)
DELETE FROM `wolf`;
/*!40000 ALTER TABLE `wolf` DISABLE KEYS */;
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES
  (1, 'Ace', '2019-07-23 17:12:26', -33.861034, 151.171936, 1),
  (2, 'Apollo', '2019-07-23 17:21:10', -33.898113, 151.174469, 1),
  (3, 'Elvis', '2019-07-23 17:25:44', -33.910751, 151.194168, 1),
  (4, 'Rocky', '2019-07-23 17:26:05', -33.881123, 151.209656, 2),
  (5, 'Buster', '2019-07-23 17:30:15', -33.874737, 151.215530, 2);
/*!40000 ALTER TABLE `wolf` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

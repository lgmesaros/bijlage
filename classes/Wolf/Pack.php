<?php 
/**
 * @package    Davison
 * @author     Davison Pro <davisonpro.coder@gmail.com>
 * @copyright  2018 Davison
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Davison\Wolf;

use Db;
use Davison\Database\DbQuery;
use Davison\ObjectModel;

class Pack extends ObjectModel {
	/** @var $id Pack ID */
	public $id;

	/** @var string $name */
	public $name;

	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'pack',
        'primary' => 'pack_id',
        'fields' => array(
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 50)
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}
<?php 
/**
 * @package    Davison
 * @author     Davison Pro <davisonpro.coder@gmail.com>
 * @copyright  2018 Davison
 * @version    1.0.0
 * @since      File available since Release 1.0.0
 */

namespace Davison\Wolf;

use Db;
use Davison\Database\DbQuery;
use Davison\ObjectModel;

class Wolf extends ObjectModel {
	/** @var $id Wolf ID */
	public $id;

	/** @var int $pack_id */
	public $pack_id;
	
	/** @var string $name */
	public $name;

	/** @var date $birthdate */
	public $birthdate;
	
	/** @var float $lat */
	public $lat;    
	
	/** @var float $lng */
    public $lng;

	/**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'wolf',
        'primary' => 'wolf_id',
        'fields' => array(
			'pack_id' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 11),
			'name' => array('type' => self::TYPE_STRING, 'required' => true, 'validate' => 'isString', 'size' => 50),
			'birthdate' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'lat' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
			'lng' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat')
        )
    );

     /**
     * constructor.
     *
     * @param null $id
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
	}
}
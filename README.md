# PHP Rest API Skeleton Project (build on Slim Framework)
https://github.com/Davisonpro/advanced-rest-api


## Software Requirements

1. Install Apache, MySQL, PHP (>= 5.4)
2. Create Virtual Host
3. Install Postman (optionally)

---

## Installation Steps

1. Clone project from repo
2. cd into project directory
3. composer install

---

## Database Setup
* Create new database `bijlage_db`
* Change database credentials in /index.php
* Create `wolf` table
```sql
CREATE TABLE IF NOT EXISTS `wolf` (
    `wolf_id` INT(11) NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `birthdate` DATETIME NOT NULL,
    `lat` FLOAT(10,6) NOT NULL,
    `lng` FLOAT(10,6) NOT NULL,
    `pack_id` INT(11) NOT NULL,
    PRIMARY KEY (`wolf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1;
```
* Dump data for `wolf` table (optionally)
```sql
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES ('1', 'Ace', '2019-07-23 17:12:26', '-33.861034', '151.171936', '1');
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES ('2', 'Apollo', '2019-07-23 17:21:10', '-33.898113', '151.174469', '1');
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES ('3', 'Elvis', '2019-07-23 17:25:44', '-33.910751', '151.194168', '1');
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES ('4', 'Rocky', '2019-07-23 17:26:05', '-33.881123', '151.209656', '2');
INSERT INTO `wolf` (`wolf_id`, `name`, `birthdate`, `lat`, `lng`, `pack_id`) VALUES ('5', 'Buster', '2019-07-23 17:30:15', '-33.874737', '151.215530', '2');
```
* Create `pack` table
```sql
CREATE TABLE IF NOT EXISTS `pack` (
 	`pack_id` INT(11) NOT NULL AUTO_INCREMENT,
  	`name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`pack_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT = 1;
```

* Dump data for `pack` table (optionally)
```sql
INSERT INTO `pack` (`pack_id`, `name`) VALUES ('1', 'The Anonymous');
INSERT INTO `pack` (`pack_id`, `name`) VALUES ('2', 'The Sniffers');
```

---

### Full database dump in /database.sql

---

## API Routes
* GET -- /api/v1/wolfs
* GET -- /api/v1/wolfs/{:id}
* POST -- /api/v1/wolfs
* PATCH -- /api/v1/wolfs/{:id}
* DELETE -- /api/v1/wolfs/{:id}
* GET -- /api/v1/packs
* POST -- /api/v1/packs